let diff_tier = [
    0,  // 0 cells
    2,  // 1 cell
    5,  // 2 cells
    8,  // 3 cells
    11  // 4 cells
];

let level_tier = [
    [1, 0],   // PrisonStart
    [3, 0],   // PrisonCourtyard
    [7, 2],   // PrisonDepths
    [3, 2],   // SewerShort
    [6, 2],   // PrisonRoof
    [8, 2],   // Ossuary
    [7, 3],   // SewerDepths
    [11, 0],  // Bridge
    [11, 0],  // Beholder
    // [9, 0],   // PrisonCorrupt
    [11, 2],  // StiltVillage
    [12, 2],  // AncientTemple
    [12, 2],  // Cemetery
    [15, 2],  // Crypt
    [16, 2],  // ClockTower
    // [13, 0],  // Cavern
    [21, 0],  // TopClockTower
    // [16, 0],  // Giant
    [22, 2],  // Castle
    [26, 0]  // Throne
];

let mob_hp = [
    120,    // Zombie
    90,     // FastZombie
    140,    // FlyZombie
    45,     // S_Fly
    140,
    30,
    50,
    100,
    100,    // Scorpio
    390,
    100,
    110,
    60,
    140,
    160,
    230,    // LeapingDuelyst
    100,
    125,
    150,
    1,
    1,
    40,
    150,    // Shielder
    215,
    220,
    200,
    1000,
    35,
    250,
    200,    // PirateChief
    195,
    90,
    100,
    200,
    400,
    190,
    300,
    150,
    4096,
    4096,
    250,
    2500,
    5900
];

let stats = [1, 1, 1];
let diff = 0;
let mob = -1;
let level = -1;

function separator_gen(val) {
  return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}

function recalc() {
    if(mob == -1 || level == -1) {
        return;
    }

    let result = 0;
    let result1 = 0;
    let result2 = 0;
    let allstats = Number(stats[0]) + Number(stats[1]) + Number(stats[2]) - 3;
    let current_diff = diff_tier[diff];
    let current_mob_hp = mob_hp[mob];
    let current_level_mintier = Number(current_diff) + Number(level_tier[level][0]);
    let current_level_maxtier = Number(current_diff + level_tier[level][0]) + Number(level_tier[level][1]);

    // alert(current_mob_hp + ' ' + current_level_mintier + ' ' + current_level_maxtier + ' ' + allstats);

    result1 = Math.round(current_mob_hp * (1 + current_level_mintier * .12) * Math.pow(1.08, allstats));
    result2 = Math.round(current_mob_hp * (1 + current_level_maxtier * .12) * Math.pow(1.08, allstats));

    let hardcap = null;
    let result_hardcap = 0;
    switch(Number(mob)) {
        case 38: hardcap = .05; break;
        case 39: hardcap = .05; break;
        case 40: hardcap = .25; break;
        case 41: hardcap = 0.03; break;
        case 42: hardcap = .05; break;
        default: hardcap = .0;
    }

    if(result1 === result2) {
        result = separator_gen(result1);
        result_hardcap = Math.round(hardcap * result1);
    }
    else {
        result = separator_gen(result1) + "-" + separator_gen(result2);
        result_hardcap = separator_gen(Math.round(hardcap * result1)) + "-" + separator_gen(Math.round(hardcap * result2));
    }

    result_hardcap += ' (' + (hardcap * 100) + '%)';

    let capdiv = document.getElementById('capdiv');

    if(hardcap > .0) {
        capdiv.style.display = "block";
    }
    else {
        capdiv.style.display = "none";
    }

    document.getElementById('result').innerHTML = result;
    document.getElementById('hardcap').innerHTML = result_hardcap;
}

function update_stat(type, val) {
    stats[type] = val;
    recalc(false);
}

function update_diff(val) {
    diff = val;
    recalc();
}

function update_mob(val) {
    mob = val;
    recalc();
}

function update_level(val) {
    level = val;
    recalc();
}

function init() {
    reset();
    stats[0] = document.getElementById('brut').value;
    stats[1] = document.getElementById('tact').value;
    stats[2] = document.getElementById('surv').value;
}

function reset() {
    document.getElementById('frm').reset();
    document.getElementById('result').innerHTML = '—';
    document.getElementById('capdiv').style.display = "none";
    stats = [1, 1, 1];
    diff = 0;
    mob = -1;
    level = -1;
}
